const request = require("supertest");
const app = require("./app/index");
const tokenAdmin =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NywibmFtZSI6ImJpbmFyIiwiZW1haWwiOiJiaW5hckBnbWFpbC5jb20iLCJpbWFnZSI6bnVsbCwicm9sZSI6eyJpZCI6MiwibmFtZSI6IkFETUlOIn0sImlhdCI6MTY1NDc4NDAxMX0.OJ7p2Iwzl0DijQDFqZIJf9wW3PGfh5t1Mee77WnpanI";
const tokenCustomer =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6OCwibmFtZSI6InVzZXIiLCJlbWFpbCI6InVzZXJAZ21haWwuY29tIiwiaW1hZ2UiOm51bGwsInJvbGUiOnsiaWQiOjEsIm5hbWUiOiJDVVNUT01FUiJ9LCJpYXQiOjE2NTQ3ODM4NTh9.umeTQDC8u9IOp4sb33T0bqV9hxqisCj9Q0_a_odxIoE";

describe("Root API", () => {
  it("GET / --> data root", () => {
    return request(app).get("/").expect(200);
  });
  it("GET / --> data root not found", () => {
    return request(app)
      .get("/iadai")
      .expect(404)
      .then((res) => {
        expect(res.body).toEqual(
          expect.objectContaining({
            error: expect.any(Object),
          })
        );
      });
  });
  it("GET / --> data root error", () => {
    return request(app)
      .post("/v1/cars/100000/rent")
      .set("Authorization", `Bearer ${tokenCustomer}`)
      .send({
        rentStartedAt: "2022-06-09",
      })
      .expect("Content-Type", /json/)
      .expect(500);
  });
});

describe("Cars API", () => {
  it("GET /v1/cars --> Get data cars", () => {
    return request(app)
      .get("/v1/cars")
      .expect("Content-Type", /json/)
      .expect(200)
      .then((res) => {
        expect(res.body).toEqual(
          expect.objectContaining({
            cars: expect.any(Array),
            meta: expect.any(Object),
          })
        );
      });
  });
  it("GET /v1/cars/id --> Get data car by id", () => {
    return request(app)
      .get("/v1/cars/1")
      .expect("Content-Type", /json/)
      .expect(200)
      .then((response) => {
        expect(response.body).toHaveProperty("createdAt");
        expect(response.body).toHaveProperty("id");
        expect(response.body).toHaveProperty("image");
        expect(response.body).toHaveProperty("isCurrentlyRented");
        expect(response.body).toHaveProperty("name");
        expect(response.body).toHaveProperty("size");
        expect(response.body).toHaveProperty("updatedAt");
      });
  });
  it("POST /v1/cars --> Create data car", () => {
    return request(app)
      .post("/v1/cars")
      .set("Authorization", `Bearer ${tokenAdmin}`)
      .send({
        name: "VW CLassic",
        price: 300000,
        size: "Small",
        image: "https://source.unsplash.com/531x531",
        isCurrentlyRented: false,
      })
      .expect("Content-Type", /json/)
      .expect(201)
      .then((response) => {
        expect(response.body).toHaveProperty("createdAt");
        expect(response.body).toHaveProperty("id");
        expect(response.body).toHaveProperty("image");
        expect(response.body).toHaveProperty("isCurrentlyRented");
        expect(response.body).toHaveProperty("name");
        expect(response.body).toHaveProperty("size");
        expect(response.body).toHaveProperty("updatedAt");
      });
  });
  it("POST /v1/cars --> Create data car Error", () => {
    function auth() {
      request(app)
        .post("/v1/auth/login")
        .send({ email: "fakhri@gmail.com", password: "fakhri" })
        .end(function (err, res) {
          token = res.body.accessToken;
        });
      return token;
    }

    return request(app)
      .post("/v1/cars")
      .set("Authorization", `Bearer ${tokenAdmin}`)
      .send({
        id: 1,
        name: "classic",
        price: true,
        size: "Small",
        image: "https://source.unsplash.com/531x531",
        isCurrentlyRented: "yoii",
      })
      .expect("Content-Type", /json/)
      .expect(422);
  });
  it("POST /v1/cars/id --> Rent Car", () => {
    return request(app)
      .post("/v1/cars/28/rent")
      .set("Authorization", `Bearer ${tokenCustomer}`)
      .send({
        rentStartedAt: "2022-06-09",
      })
      .expect("Content-Type", /json/)
      .expect(201);
  });

  it("POST /v1/cars/id --> Rent Car Already Rented", () => {
    return request(app)
      .post("/v1/cars/7/rent")
      .set("Authorization", `Bearer ${tokenCustomer}`)
      .send({
        rentStartedAt: "2022-06-09",
      })
      .expect("Content-Type", /json/)
      .expect(500);
  });

  it("POST /v1/cars/id --> Rent Car Already Error", () => {
    return request(app)
      .post("/v1/cars//rent")
      .set("Authorization", `Bearer ${tokenCustomer}`)
      .send({
        id: "2022-06-09",
      })
      .expect("Content-Type", /json/);
  });
  it("PUT /v1/cars/:id --> Update data car", () => {
    return request(app)
      .put("/v1/cars/5")
      .set("Authorization", `Bearer ${tokenAdmin}`)
      .send({
        name: "VW CLassic",
        price: 300000,
        size: "Small",
        image: "https://source.unsplash.com/531x531",
        isCurrentlyRented: false,
      })
      .expect("Content-Type", /json/)
      .expect(200)
      .then((response) => {
        expect(response.body).toHaveProperty("createdAt");
        expect(response.body).toHaveProperty("id");
        expect(response.body).toHaveProperty("image");
        expect(response.body).toHaveProperty("isCurrentlyRented");
        expect(response.body).toHaveProperty("name");
        expect(response.body).toHaveProperty("size");
        expect(response.body).toHaveProperty("updatedAt");
      });
  });

  it("DELETE /v1/cars/:id --> Delete data car", () => {
    return request(app)
      .delete("/v1/cars/19")
      .set("Authorization", `Bearer ${tokenAdmin}`)
      .expect(204);
  });
});

describe("Auth API", () => {
  it("POST / --> login", () => {
    return request(app)
      .post("/v1/auth/login")
      .send({
        email: "binar@gmail.com",
        password: "binar",
      })
      .expect("Content-Type", /json/)
      .expect(201);
  });

  it("POST / --> login email not registered", () => {
    return request(app)
      .post("/v1/auth/login")
      .send({
        email: "testemail@gmail.com",
        password: "binar",
      })
      .expect("Content-Type", /json/)
      .expect(404);
  });

  it("POST / --> login password wrong", () => {
    return request(app)
      .post("/v1/auth/login")
      .send({
        email: "binar@gmail.com",
        password: "binur",
      })
      .expect("Content-Type", /json/)
      .expect(401);
  });

  it("POST / --> login errror", () => {
    return request(app)
      .post("/v1/auth/login")
      .send({
        email: "",
        password: "",
      })
      .expect("Content-Type", /json/)
      .expect(404);
  });

  it("POST / --> Register", () => {
    return request(app)
      .post("/v1/auth/register")
      .send({
        name: "test3",
        email: "test3@gmail.com",
        password: "test3",
      })
      .expect("Content-Type", /json/)
      .expect(201);
  });

  it("POST / --> Register Email Already Taken", () => {
    return request(app)
      .post("/v1/auth/register")
      .send({
        name: "ricky",
        email: "binar@gmail.com",
        password: "ricky",
      })
      .expect("Content-Type", /json/)
      .expect(500);
  });

  it("POST / --> Get User Profile", () => {
    return request(app)
      .get("/v1/auth/whoami")
      .set("Authorization", `Bearer ${tokenCustomer}`)
      .expect("Content-Type", /json/)
      .expect(200)
      .then((response) => {
        expect(response.body).toHaveProperty("createdAt");
        expect(response.body).toHaveProperty("id");
        expect(response.body).toHaveProperty("image");
        expect(response.body).toHaveProperty("email");
        expect(response.body).toHaveProperty("name");
        expect(response.body).toHaveProperty("updatedAt");
      });
  });

  it("POST / --> Get User Profile Error Not Found", () => {
    return request(app)
      .get("/v1/auth/whomqi")
      .set("Authorization", `Bearer ${tokenCustomer}`)
      .expect("Content-Type", /json/)
      .expect(404)
      .then((res) => {
        expect(res.body).toEqual(
          expect.objectContaining({
            error: expect.any(Object),
          })
        );
      });
  });

  it("POST / --> Get User Profile Error Role Admin", () => {
    return request(app)
      .get("/v1/auth/whoami")
      .set("Authorization", `Bearer ${tokenAdmin}`)
      .expect("Content-Type", /json/)
      .expect(401)
      .then((res) => {
        expect(res.body).toEqual(
          expect.objectContaining({
            error: expect.any(Object),
          })
        );
      });
  });
});
